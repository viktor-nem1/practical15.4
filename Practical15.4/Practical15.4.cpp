﻿#include <iostream>

void Numbers(bool x, int n) 
{
	int a = x ? 0 : 1;
	
	for (int i = a; i <= n; i += 2) 
	{
		std::cout << i << " ";
	}
	
	std::cout << std::endl;
}

int main() 
{
	const int N = 30;

	std::cout << "Even numbers " << std::endl;
	Numbers(true, N);

	std::cout << "Odd numbers " << std::endl;
	Numbers(false, N);

	return 0;
}